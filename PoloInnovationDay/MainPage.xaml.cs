﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Plugin.Connectivity;
using PoloInnovationDay.Models;
using PoloInnovationDay.Services;
using PoloInnovationDay.Utility;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace PoloInnovationDay
{

    public partial class MainPage : ContentPage
    {
        RootObject oggetto;
        string currentUrl;

        public MainPage()
        {
            InitializeComponent();


            Xamarin.Forms.NavigationPage.SetHasNavigationBar(this, false);
            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);

            var tapGesture = new Xamarin.Forms.TapGestureRecognizer();
            tapGesture.Tapped += TapGesture_Tapped;

            img_back.GestureRecognizers.Add(tapGesture);
            img_home.GestureRecognizers.Add(tapGesture);

            web_view.Navigated += Web_View_Navigated;
            web_view.Navigating += Web_View_Navigating;

        }




        protected override void OnAppearing()
        {
            base.OnAppearing();
            Init();
        }


        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);

            masterPage.Init();
        }



        void Web_View_Navigating(object sender, WebNavigatingEventArgs e)
        {

            //loading.IsVisible = true;
        }


        void Web_View_Navigated(object sender, WebNavigatedEventArgs e)
        {
            loading.IsVisible = false;
            currentUrl = e.Url;

            if(e.Url == Sistema.URL_DOMINIO + "?device=" + Device.RuntimePlatform.ToLower() + "&versione=" + DependencyService.Get<IAppVersion>().GetBuild())
            {
                //Sono alla home page
                gridMenu.IsVisible = false;
                l_titolo.Text = "";
            }
            else
            {
                gridMenu.IsVisible = true;
            }

        }


        async void Init()
        {
            try
            {
                //Per far funzionare gli alert asincroni
                await Task.Delay(500);

                if (!CrossConnectivity.Current.IsConnected)
                {
                    await DisplayAlert("Attenzione", "Connessione internet assente", "OK");
                    //Chiudo l'applicazione
                    DependencyService.Get<ICloseApp>().CloseApplication();

                    return;
                }


                oggetto = await RestApi.GetData();
                if (oggetto == null)
                {
                    CreaAlert("Si è verificato un errore");
                    return;
                }

                if (Sistema.AggiornamentoRichiesto(oggetto))
                {
                    CreaAlert("Si è verificato un errore");
                    return;
                }

                masterPage.Setup(oggetto);
                masterPage.pageContent = this;
                //loading.IsVisible = true;
                SetUrl(Sistema.URL_DOMINIO);

            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                CreaAlert("Si è verificato un errore");
                return;
            }
        }




        void TapGesture_Tapped(object sender, EventArgs e)
        {
            string nome_componente = sender.GetType().Name;

            if (nome_componente.Equals("Image"))
            {
                Image image = (Image)sender;
                string class_name = image.ClassId;

                if (class_name.Equals("img_back"))
                {
                    if (web_view.CanGoBack && currentUrl != Sistema.URL_DOMINIO)
                    {
                        web_view.GoBack();
                    }
                }
                else if(class_name.Equals("img_home"))
                {
                    SetUrl(Sistema.URL_DOMINIO);
                }
            }
        }




        public void SetTitoloNavBar(string titolo)
        {
            if(!string.IsNullOrWhiteSpace(titolo))
            {
                l_titolo.Text = titolo;
            }
        }



        public async void SetUrl(string url)
        {
            if(!string.IsNullOrWhiteSpace(url))
            {
                if (!CrossConnectivity.Current.IsConnected)
                {
                    await DisplayAlert("Attenzione", "Connessione internet assente", "OK");
                    //Chiudo l'applicazione
                    DependencyService.Get<ICloseApp>().CloseApplication();

                    return;
                }

                //loading.IsVisible = true;

                web_view.Source = url + "?device=" + Device.RuntimePlatform.ToLower() + "&versione=" + DependencyService.Get<IAppVersion>().GetBuild();
            }
        }



        public async void CreaAlert(string messaggio)
        {
            await DisplayAlert("Attenzione", messaggio, "OK");
        }


    }
}
