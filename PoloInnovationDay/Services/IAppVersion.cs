﻿using System;
namespace PoloInnovationDay.Services
{
    public interface IAppVersion
    {
        string GetVersion();
        string GetBuild();
    }
}
