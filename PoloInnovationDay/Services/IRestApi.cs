﻿using System;
using System.Threading.Tasks;
using PoloInnovationDay.Models;
using Refit;

namespace PoloInnovationDay
{
    public interface IRestApi
    {
        [Get("/config_json.php")]
        Task<RootObject> GetData();

    }
}
