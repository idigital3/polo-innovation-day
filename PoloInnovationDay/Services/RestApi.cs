﻿using System;
using System.Threading.Tasks;
using PoloInnovationDay.Models;
using PoloInnovationDay.Utility;

namespace PoloInnovationDay
{
    public class RestApi
    {

        internal static IRestApi RefitApi { get; private set; }

        static RestApi()
        {
            RefitApi = Refit.RestService.For<IRestApi>(Sistema.URL_DOMINIO);
        }


        internal static async Task<RootObject> GetData()
        => await RefitApi.GetData();


    }
}
