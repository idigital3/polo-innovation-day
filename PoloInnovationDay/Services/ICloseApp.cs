﻿using System;
namespace PoloInnovationDay.Services
{
    public interface ICloseApp
    {
        void CloseApplication();
    }
}
