﻿using System;
namespace PoloInnovationDay.Models
{
    public class TopMenu
    {
        public MenuPolocosmesi polocosmesi { get; set; }
        public MenuInnovationDay innovationday { get; set; }
    }
}
