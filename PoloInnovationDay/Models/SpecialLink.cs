﻿using System;
namespace PoloInnovationDay.Models
{
    public class SpecialLink
    {
        public string label { get; set; }
        public string url { get; set; }
        public bool enabled { get; set; }
        public bool required_login { get; set; }
        public string navbar_title { get; set; }
        public bool show_if_logged { get; set; }
        public string color { get; set; }

    }
}
