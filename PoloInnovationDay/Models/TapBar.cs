﻿using System;
namespace PoloInnovationDay.Models
{
    public class TapBar
    {
        public string label { get; set; }
        public string url { get; set; }
        public bool required_login { get; set; }
        public string navbar_title { get; set; }
        public string id_top_menu { get; set; }
    }
}
