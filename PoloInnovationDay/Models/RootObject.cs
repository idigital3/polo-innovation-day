﻿using System;
using System.Collections.Generic;

namespace PoloInnovationDay.Models
{
    public class RootObject
    {
        public List<TapBar> tapbar { get; set; }
        public TopMenu top_menu { get; set; }
        public int last_update { get; set; }
        public string current_version { get; set; }
        public bool force_upgrade { get; set; }
    }
}
