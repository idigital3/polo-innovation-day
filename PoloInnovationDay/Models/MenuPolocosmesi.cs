﻿using System;
using System.Collections.Generic;

namespace PoloInnovationDay.Models
{
    public class MenuPolocosmesi
    {
        public string logo { get; set; }
        public string welcome { get; set; }
        public List<Link> link { get; set; }
        public SpecialLink special_link { get; set; }
    }
}
