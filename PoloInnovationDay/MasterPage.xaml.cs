﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using PoloInnovationDay.Models;
using PoloInnovationDay.Utility;
using Xamarin.Forms;

namespace PoloInnovationDay
{
    [ContentProperty("ContainerContent")]
    public partial class MasterPage : ContentView
    {
        RootObject oggetto;
        public MainPage pageContent { get; set; }

        public MasterPage()
        {
            InitializeComponent();

            var tapGesture = new Xamarin.Forms.TapGestureRecognizer();
            tapGesture.Tapped += TapGesture_Tapped;

            lblPoloCosmesi.GestureRecognizers.Add(tapGesture);
            lblInnovationDay.GestureRecognizers.Add(tapGesture);

            l_polo_1.GestureRecognizers.Add(tapGesture);
            l_polo_2.GestureRecognizers.Add(tapGesture);
            l_polo_3.GestureRecognizers.Add(tapGesture);
            l_polo_4.GestureRecognizers.Add(tapGesture);
            l_polo_5.GestureRecognizers.Add(tapGesture);
            l_polo_6.GestureRecognizers.Add(tapGesture);
            l_polo_7.GestureRecognizers.Add(tapGesture);
            l_polo_8.GestureRecognizers.Add(tapGesture);
            l_polo_9.GestureRecognizers.Add(tapGesture);
            l_polo_10.GestureRecognizers.Add(tapGesture);

            l_inno_1.GestureRecognizers.Add(tapGesture);
            l_inno_2.GestureRecognizers.Add(tapGesture);
            l_inno_3.GestureRecognizers.Add(tapGesture);
            l_inno_4.GestureRecognizers.Add(tapGesture);
            l_inno_5.GestureRecognizers.Add(tapGesture);
            l_inno_6.GestureRecognizers.Add(tapGesture);
            l_inno_7.GestureRecognizers.Add(tapGesture);
            l_inno_8.GestureRecognizers.Add(tapGesture);
            l_inno_9.GestureRecognizers.Add(tapGesture);
            l_inno_10.GestureRecognizers.Add(tapGesture);

            stackMenu.GestureRecognizers.Add(tapGesture);

            btnProfilo.Clicked += BtnProfilo_Clicked;
            btnAreaRiservata.Clicked += BtnAreaRiservata_Clicked;
        }



        public View ContainerContent
        {
            get { return ContentFrame.Content; }
            set { ContentFrame.Content = value; }
        }


        public void Init()
        {
            stackMenu.TranslateTo(0, stackMenu.Height, 0);

            //stackMenu_Profilo.HeightRequest = stackMenu.Height - ((stackMenu.Height * 20) / 100);
            //stackMenu_AreaRiservata.HeightRequest = stackMenu.Height - ((stackMenu.Height * 20) / 100);
        }



        public void Setup(RootObject oggetto)
        {
            this.oggetto = oggetto;

            lblPoloCosmesi.Text = oggetto.tapbar[0].label;
            lblInnovationDay.Text = oggetto.tapbar[1].label;

            if (!string.IsNullOrWhiteSpace(Sistema.URL_DOMINIO + oggetto.top_menu.polocosmesi.logo))
                img_polo.Source = Sistema.URL_DOMINIO + oggetto.top_menu.polocosmesi.logo;

            if (!string.IsNullOrWhiteSpace(Sistema.URL_DOMINIO + oggetto.top_menu.innovationday.logo))
                img_innovation.Source = Sistema.URL_DOMINIO + oggetto.top_menu.innovationday.logo;



            List<Link> listaVociMenuPolo = oggetto.top_menu.polocosmesi.link.OrderBy(x => x.enabled == true).ToList();
            List<Link> listaVociMenuInnovation = oggetto.top_menu.innovationday.link.OrderBy(x => x.enabled == true).ToList();

            btnAreaRiservata.BackgroundColor = Color.FromHex(oggetto.top_menu.polocosmesi.special_link.color);
            btnAreaRiservata.Text = oggetto.top_menu.polocosmesi.special_link.label;
            btnAreaRiservata.IsVisible = oggetto.top_menu.polocosmesi.special_link.enabled;

            btnProfilo.BackgroundColor = Color.FromHex(oggetto.top_menu.innovationday.special_link.color);
            btnProfilo.Text = oggetto.top_menu.innovationday.special_link.label;
            btnProfilo.IsVisible = oggetto.top_menu.innovationday.special_link.enabled;

            //MENU POLO

            if (listaVociMenuPolo[0].enabled)
            {
                l_polo_1.Text = listaVociMenuPolo[0].label;
                l_polo_1.IsVisible = true;
            }
            if (listaVociMenuPolo[1].enabled)
            {
                l_polo_2.Text = listaVociMenuPolo[1].label;
                l_polo_2.IsVisible = true;
            }
            if (listaVociMenuPolo[2].enabled)
            {
                l_polo_3.Text = listaVociMenuPolo[2].label;
                l_polo_3.IsVisible = true;
            }
            if (listaVociMenuPolo[3].enabled)
            {
                l_polo_4.Text = listaVociMenuPolo[3].label;
                l_polo_4.IsVisible = true;
            }
            if (listaVociMenuPolo[4].enabled)
            {
                l_polo_5.Text = listaVociMenuPolo[4].label;
                l_polo_5.IsVisible = true;
            }
            if (listaVociMenuPolo[5].enabled)
            {
                l_polo_6.Text = listaVociMenuPolo[5].label;
                l_polo_6.IsVisible = true;
            }
            if (listaVociMenuPolo[6].enabled)
            {
                l_polo_7.Text = listaVociMenuPolo[6].label;
                l_polo_7.IsVisible = true;
            }
            if (listaVociMenuPolo[7].enabled)
            {
                l_polo_8.Text = listaVociMenuPolo[7].label;
                l_polo_8.IsVisible = true;
            }
            if (listaVociMenuPolo[8].enabled)
            {
                l_polo_9.Text = listaVociMenuPolo[8].label;
                l_polo_9.IsVisible = true;
            }
            if (listaVociMenuPolo[9].enabled)
            {
                l_polo_10.Text = listaVociMenuPolo[9].label;
                l_polo_10.IsVisible = true;
            }

            //MENU INNOVATION

            if (listaVociMenuInnovation[0].enabled)
            {
                l_inno_1.Text = listaVociMenuInnovation[0].label;
                l_inno_1.IsVisible = true;
            }
            if (listaVociMenuInnovation[1].enabled)
            {
                l_inno_2.Text = listaVociMenuInnovation[1].label;
                l_inno_2.IsVisible = true;
            }
            if (listaVociMenuInnovation[2].enabled)
            {
                l_inno_3.Text = listaVociMenuInnovation[2].label;
                l_inno_3.IsVisible = true;
            }
            if (listaVociMenuInnovation[3].enabled)
            {
                l_inno_4.Text = listaVociMenuInnovation[3].label;
                l_inno_4.IsVisible = true;
            }
            if (listaVociMenuInnovation[4].enabled)
            {
                l_inno_5.Text = listaVociMenuInnovation[4].label;
                l_inno_5.IsVisible = true;
            }
            if (listaVociMenuInnovation[5].enabled)
            {
                l_inno_6.Text = listaVociMenuInnovation[5].label;
                l_inno_6.IsVisible = true;
            }
            if (listaVociMenuInnovation[6].enabled)
            {
                l_inno_7.Text = listaVociMenuInnovation[6].label;
                l_inno_7.IsVisible = true;
            }
            if (listaVociMenuInnovation[7].enabled)
            {
                l_inno_8.Text = listaVociMenuInnovation[7].label;
                l_inno_8.IsVisible = true;
            }
            if (listaVociMenuInnovation[8].enabled)
            {
                l_inno_9.Text = listaVociMenuInnovation[8].label;
                l_inno_9.IsVisible = true;
            }
            if (listaVociMenuInnovation[9].enabled)
            {
                l_inno_10.Text = listaVociMenuInnovation[9].label;
                l_inno_10.IsVisible = true;
            }

        }



        async void TapGesture_Tapped(object sender, EventArgs e)
        {
            string nome_componente = sender.GetType().Name;

            if (nome_componente.Equals("Label"))
            {
                Label l = (Label)sender;
                string class_name = l.ClassId;

                if (class_name.Equals("lblPoloCosmesi"))
                {
                    //MENU AREA RISERVATA
                    MostraNacondiMenuInnovationDay(false);
                    MostraNacondiMenuPoloCosmesi(true);
                }
                else if(class_name.Equals("lblInnovationDay"))
                {
                    //MENU PROFILO
                    MostraNacondiMenuPoloCosmesi(false);
                    MostraNacondiMenuInnovationDay(true);
                }
                else
                {
                    List<Link> listaVociMenuPolo = oggetto.top_menu.polocosmesi.link.OrderBy(x => x.enabled == true).ToList();
                    List<Link> listaVociMenuInnovation = oggetto.top_menu.innovationday.link.OrderBy(x => x.enabled == true).ToList();

                    string url = "";
                    string label = "";

                    if (class_name.Equals("l_polo_1"))
                    {
                        url = listaVociMenuPolo[0].url;
                        label = listaVociMenuPolo[0].label;
                        pageContent.SetTitoloNavBar(listaVociMenuPolo[0].navbar_title);
                    }
                    else if (class_name.Equals("l_polo_2"))
                    {
                        url = listaVociMenuPolo[1].url;
                        label = listaVociMenuPolo[1].label;
                        pageContent.SetTitoloNavBar(listaVociMenuPolo[1].navbar_title);
                    }
                    else if (class_name.Equals("l_polo_3"))
                    {
                        url = listaVociMenuPolo[2].url;
                        label = listaVociMenuPolo[2].label;
                        pageContent.SetTitoloNavBar(listaVociMenuPolo[2].navbar_title);
                    }
                    else if (class_name.Equals("l_polo_4"))
                    {
                        url = listaVociMenuPolo[3].url;
                        label = listaVociMenuPolo[3].label;
                        pageContent.SetTitoloNavBar(listaVociMenuPolo[3].navbar_title);
                    }
                    else if (class_name.Equals("l_polo_5"))
                    {
                        url = listaVociMenuPolo[4].url;
                        label = listaVociMenuPolo[4].label;
                        pageContent.SetTitoloNavBar(listaVociMenuPolo[4].navbar_title);
                    }
                    else if (class_name.Equals("l_polo_6"))
                    {
                        url = listaVociMenuPolo[5].url;
                        label = listaVociMenuPolo[5].label;
                        pageContent.SetTitoloNavBar(listaVociMenuPolo[5].navbar_title);
                    }
                    else if (class_name.Equals("l_polo_7"))
                    {
                        url = listaVociMenuPolo[6].url;
                        label = listaVociMenuPolo[6].label;
                        pageContent.SetTitoloNavBar(listaVociMenuPolo[6].navbar_title);
                    }
                    else if (class_name.Equals("l_polo_8"))
                    {
                        url = listaVociMenuPolo[7].url;
                        label = listaVociMenuPolo[7].label;
                        pageContent.SetTitoloNavBar(listaVociMenuPolo[7].navbar_title);
                    }
                    else if (class_name.Equals("l_polo_9"))
                    {
                        url = listaVociMenuPolo[8].url;
                        label = listaVociMenuPolo[8].label;
                        pageContent.SetTitoloNavBar(listaVociMenuPolo[8].navbar_title);
                    }
                    else if (class_name.Equals("l_polo_10"))
                    {
                        url = listaVociMenuPolo[9].url;
                        label = listaVociMenuPolo[9].label;
                        pageContent.SetTitoloNavBar(listaVociMenuPolo[9].navbar_title);
                    }
                    else if (class_name.Equals("l_inno_1"))
                    {
                        url = listaVociMenuInnovation[0].url;
                        label = listaVociMenuInnovation[0].label;
                        pageContent.SetTitoloNavBar(listaVociMenuInnovation[0].navbar_title);
                    }
                    else if (class_name.Equals("l_inno_2"))
                    {
                        url = listaVociMenuInnovation[1].url;
                        label = listaVociMenuInnovation[1].label;
                        pageContent.SetTitoloNavBar(listaVociMenuInnovation[1].navbar_title);
                    }
                    else if (class_name.Equals("l_inno_3"))
                    {
                        url = listaVociMenuInnovation[2].url;
                        label = listaVociMenuInnovation[2].label;
                        pageContent.SetTitoloNavBar(listaVociMenuInnovation[2].navbar_title);
                    }
                    else if (class_name.Equals("l_inno_4"))
                    {
                        url = listaVociMenuInnovation[3].url;
                        label = listaVociMenuInnovation[3].label;
                        pageContent.SetTitoloNavBar(listaVociMenuInnovation[3].navbar_title);
                    }
                    else if (class_name.Equals("l_inno_5"))
                    {
                        url = listaVociMenuInnovation[4].url;
                        label = listaVociMenuInnovation[4].label;
                        pageContent.SetTitoloNavBar(listaVociMenuInnovation[4].navbar_title);
                    }
                    else if (class_name.Equals("l_inno_6"))
                    {
                        url = listaVociMenuInnovation[5].url;
                        label = listaVociMenuInnovation[5].label;
                        pageContent.SetTitoloNavBar(listaVociMenuInnovation[5].navbar_title);
                    }
                    else if (class_name.Equals("l_inno_7"))
                    {
                        url = listaVociMenuInnovation[6].url;
                        label = listaVociMenuInnovation[6].label;
                        pageContent.SetTitoloNavBar(listaVociMenuInnovation[6].navbar_title);
                    }
                    else if (class_name.Equals("l_inno_8"))
                    {
                        url = listaVociMenuInnovation[7].url;
                        label = listaVociMenuInnovation[7].label;
                        pageContent.SetTitoloNavBar(listaVociMenuInnovation[7].navbar_title);
                    }
                    else if (class_name.Equals("l_inno_9"))
                    {
                        url = listaVociMenuInnovation[8].url;
                        label = listaVociMenuInnovation[8].label;
                        pageContent.SetTitoloNavBar(listaVociMenuInnovation[8].navbar_title);
                    }
                    else if (class_name.Equals("l_inno_10"))
                    {
                        url = listaVociMenuInnovation[9].url;
                        label = listaVociMenuInnovation[9].label;
                        pageContent.SetTitoloNavBar(listaVociMenuInnovation[9].navbar_title);
                    }

                    if(!string.IsNullOrWhiteSpace(url))
                        pageContent.SetUrl(Sistema.URL_DOMINIO + url);

                    ChiudiMenu();
                }

            }
            else if(nome_componente.Equals("StackLayout"))
            {
                StackLayout stack = (StackLayout)sender;
                string class_name = stack.ClassId;

                if (class_name.Equals("stackMenu"))
                {
                    await stackMenu.TranslateTo(0, stackMenu.Height, 400);

                    MostraNacondiMenuPoloCosmesi(false);
                    MostraNacondiMenuInnovationDay(false);
                }
            }
        }


        void BtnProfilo_Clicked(object sender, EventArgs e)
        {
            //Innovation day

            pageContent.SetTitoloNavBar(oggetto.top_menu.innovationday.special_link.navbar_title);
            pageContent.SetUrl(Sistema.URL_DOMINIO + oggetto.top_menu.innovationday.special_link.url);

            ChiudiMenu();

        }

        void BtnAreaRiservata_Clicked(object sender, EventArgs e)
        {
            //Polo

            pageContent.SetTitoloNavBar(oggetto.top_menu.polocosmesi.special_link.navbar_title);
            pageContent.SetUrl(Sistema.URL_DOMINIO + oggetto.top_menu.polocosmesi.special_link.url);

            ChiudiMenu();

        }


        void MostraNacondiMenuPoloCosmesi(bool Mostra)
        {
            stackMenu_AreaRiservata.IsVisible = Mostra;

            //lblPoloCosmesi.FontAttributes = FontAttributes.None;
            lblPoloCosmesi.FontFamily = Device.RuntimePlatform == Device.iOS ? "Lato" : "Lato.ttf#Lato";

            if (Mostra)
            {
                stackMenu.TranslateTo(0, 0, 400);
                //lblPoloCosmesi.FontAttributes = FontAttributes.Bold;
                lblPoloCosmesi.FontFamily = Device.RuntimePlatform == Device.iOS ? "Lato-Bold" : "Lato-Bold.ttf#Lato-Bold";
            }

        }

        void MostraNacondiMenuInnovationDay(bool Mostra)
        {
            stackMenu_Profilo.IsVisible = Mostra;

            //lblInnovationDay.FontAttributes = FontAttributes.None;
            lblInnovationDay.FontFamily = Device.RuntimePlatform == Device.iOS ? "Lato" : "Lato.ttf#Lato";
            if (Mostra)
            {
                stackMenu.TranslateTo(0, 0, 400);
                //lblInnovationDay.FontAttributes = FontAttributes.Bold;
                lblInnovationDay.FontFamily = Device.RuntimePlatform == Device.iOS ? "Lato-Bold" : "Lato-Bold.ttf#Lato-Bold";
            }

        }



        async void ChiudiMenu()
        {
            await stackMenu.TranslateTo(0, stackMenu.Height, 0);

            MostraNacondiMenuPoloCosmesi(false);
            MostraNacondiMenuInnovationDay(false);
        }


    }
}
