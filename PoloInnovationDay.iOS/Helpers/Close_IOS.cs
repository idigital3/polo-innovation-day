﻿using System;
using System.Threading;
using PoloInnovationDay.Services;

[assembly: Xamarin.Forms.Dependency(typeof(PoloInnovationDay.iOS.Helpers.Close_IOS))]
namespace PoloInnovationDay.iOS.Helpers
{
    public class Close_IOS : ICloseApp
    {
        public Close_IOS()
        {

        }

        public void CloseApplication()
        {
            Thread.CurrentThread.Abort();
        }
    }
}
