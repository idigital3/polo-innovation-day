﻿using System;
using Foundation;
using PoloInnovationDay.Services;

[assembly: Xamarin.Forms.Dependency(typeof(PoloInnovationDay.iOS.Helpers.Version_IOS))]
namespace PoloInnovationDay.iOS.Helpers
{
    public class Version_IOS : IAppVersion
    {
        public Version_IOS()
        {
        }

        public string GetBuild()
        {
            return NSBundle.MainBundle.ObjectForInfoDictionary("CFBundleVersion").ToString();
        }

        public string GetVersion()
        {
            return NSBundle.MainBundle.ObjectForInfoDictionary("CFBundleShortVersionString").ToString();
        }
    }
}
