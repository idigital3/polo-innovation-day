﻿using Foundation;
using UIKit;
//using WindowsAzure.Messaging;
//using UserNotifications;

namespace PoloInnovationDay.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        //Riferimento a hub azure per push notifications
        //private SBNotificationHub Hub { get; set; }

        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            global::Xamarin.Forms.Forms.Init();

            /*
            int SystemVersion = Convert.ToInt16(UIDevice.CurrentDevice.SystemVersion.Split('.')[0]);

            if (SystemVersion >= 10)
            {
                UNUserNotificationCenter.Current.RequestAuthorization(UNAuthorizationOptions.Alert | UNAuthorizationOptions.Badge | UNAuthorizationOptions.Sound,
                                                                        (granted, error) =>
                                                                        {
                                                                            if (granted)
                                                                                InvokeOnMainThread(UIApplication.SharedApplication.RegisterForRemoteNotifications);

                                                                        });
            }
            else if (SystemVersion >= 8)
            {
                var pushSettings = UIUserNotificationSettings.GetSettingsForTypes(
                        UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound,
                        new NSSet());

                UIApplication.SharedApplication.RegisterUserNotificationSettings(pushSettings);
                UIApplication.SharedApplication.RegisterForRemoteNotifications();
            }
            else
            {
                UIRemoteNotificationType notificationTypes = UIRemoteNotificationType.Alert | UIRemoteNotificationType.Badge | UIRemoteNotificationType.Sound;
                UIApplication.SharedApplication.RegisterForRemoteNotificationTypes(notificationTypes);
            }


            //Tolgo il badge sull'icona dell'app
            if (UIApplication.SharedApplication.ApplicationIconBadgeNumber > 0)
            {
                UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
            }
            */

            LoadApplication(new App());

            return base.FinishedLaunching(app, options);
        }


        /*
        public override void WillEnterForeground(UIApplication application)
        {
            //Tolgo il badge sull'icona dell'app
            if (UIApplication.SharedApplication.ApplicationIconBadgeNumber > 0)
            {
                UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
            }

        }
        */


        /*
        public override void ReceivedLocalNotification(UIApplication application, UILocalNotification notification)
        {
            // show an alert
            new UIAlertView(notification.AlertAction, notification.AlertBody, null, "OK", null).Show();

            // reset our badge
            UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
        }
        */


        /*
        public override void DidRegisterUserNotificationSettings(UIApplication application, UIUserNotificationSettings notificationSettings)
        {
            Console.WriteLine("Registrazione push notifications OK!!");
        }
        */

        /*
        public override void FailedToRegisterForRemoteNotifications(UIApplication application, NSError error)
        {
            Console.WriteLine("Push subscription failed: {0}", error.LocalizedDescription);
        }
        */

        /*
        public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        {
            //base.RegisteredForRemoteNotifications(application, deviceToken);

            //Stringhe connessione hub azure test e prod
#if DEBUG
            string ConnectionString = "Endpoint=sb://pushnotificationtest-ns.servicebus.windows.net/;SharedAccessKeyName=DefaultFullSharedAccessSignature;SharedAccessKey=BL/PDU8EtLZqcGiZU3+mJnVyJHGYzG4V5+7kR6A1co8=";
            string NotificationHubPath = "pushtest";


#else
string ConnectionString = "Endpoint=sb://idigital3pushservice.servicebus.windows.net/;SharedAccessKeyName=DefaultFullSharedAccessSignature;SharedAccessKey=CewuN9Y24yidUWX0TYrcLQcYjRWPgwOfeQaPVqRGD5M=";
    string NotificationHubPath = "apppolocosmesi";
#endif


            Hub = new SBNotificationHub(ConnectionString, NotificationHubPath);

            Hub.UnregisterAll(deviceToken, (error) =>
            {
                if (error != null)
                {
                    Console.WriteLine("Error calling Unregister: {0}", error.ToString());
                    return;
                }


                NSSet tags = null;



                Hub.RegisterNative(deviceToken, tags, (errorCallback) =>
                {
                    if (errorCallback != null)
                        Console.WriteLine("RegisterNativeAsync error: " + errorCallback.ToString());
                });
            });

        }
        */

        /*
        public override void ReceivedRemoteNotification(UIApplication app, NSDictionary userInfo)
        {
            // Process a notification received while the app was already open
            ProcessNotification(userInfo, false);
            //NSNotificationCenter.DefaultCenter.PostNotificationName("NOME", this
        }
        */


        /*
        void ProcessNotification(NSDictionary options, bool fromFinishedLaunching)
        {
            // Check to see if the dictionary has the aps key.  This is the notification payload you would have sent
            if (null != options && options.ContainsKey(new NSString("aps")))
            {
                //Get the aps dictionary
                NSDictionary aps = options.ObjectForKey(new NSString("aps")) as NSDictionary;

                string alert = string.Empty;

                //Extract the alert text
                // NOTE: If you're using the simple alert by just specifying
                // "  aps:{alert:"alert msg here"}  ", this will work fine.
                // But if you're using a complex alert with Localization keys, etc.,
                // your "alert" object from the aps dictionary will be another NSDictionary.
                // Basically the JSON gets dumped right into a NSDictionary,
                // so keep that in mind.
                if (aps.ContainsKey(new NSString("alert")))
                    alert = (aps[new NSString("alert")] as NSString).ToString();


                //If this came from the ReceivedRemoteNotification while the app was running,
                // we of course need to manually process things like the sound, badge, and alert.
                if (!fromFinishedLaunching)
                {
                    //Manually show an alert
                    if (!string.IsNullOrEmpty(alert))
                    {
                        UIAlertView avAlert = new UIAlertView("Polo Cosmesi", alert, null, "OK", null);
                        avAlert.Show();
                    }
                }
            }
        }
        */

    }
}

