﻿using System;
using Android.App;
using PoloInnovationDay.Services;
using Xamarin.Forms;

[assembly: Xamarin.Forms.Dependency(typeof(PoloInnovationDay.Droid.Helpers.Close_Android))]
namespace PoloInnovationDay.Droid.Helpers
{
    public class Close_Android : ICloseApp
    {
        public Close_Android(){

        }

        public void CloseApplication()
        {

            Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
            //var activity = (Activity)Forms.Context;
            //activity.FinishAffinity();
        }
    }
}
